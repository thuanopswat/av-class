# mcl-avclass

Forked out of [AVClass](https://github.com/malicialab/avclass), [mcl-avclass](https://bitbucket.org/metascan/mcl-avclass) handles basic malware labeling using MetaDefender Cloud lookup response & specific AV settings.

For usage refer to [documentation](https://opswat.atlassian.net/wiki/spaces/MON/pages/556631727/mcl-avclass).

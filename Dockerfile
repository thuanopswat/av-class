FROM python:3

RUN pip3 install zerorpc
WORKDIR /av_class
ADD . /av_class
ENTRYPOINT [ "python", "rpc_server.py" ]

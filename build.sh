#!/bin/bash
REGIONS=('us-west-2' 'eu-central-1')
IMAGE=md-rest-av_class
VERSION=1.0.1

if [[ "${ENV}" == "prod" ]]; then
    AWS_ACC="245471307742"          # opswat-mdcthreatintel-prod
fi

REPO_PREFIX=${AWS_ACC:-"740519714208"}.dkr.ecr
REPO_SUFFIX=amazonaws.com/opswat

for REGION in "${REGIONS[@]}"
do
    docker build -t ${REPO_PREFIX}.${REGION}.${REPO_SUFFIX}/$IMAGE:$VERSION --force-rm=true .
done

echo "Upload $REPO/$IMAGE to ECR? [y,N]"

read ANSWER

if [[ ${ANSWER} == 'y' ]]; then
    for REGION in "${REGIONS[@]}"
    do
        DOCKER_PWD=`aws ecr get-login-password --region ${REGION}`
        docker login --username AWS --password $DOCKER_PWD ${REPO_PREFIX}.${REGION}.amazonaws.com
        docker push ${REPO_PREFIX}.${REGION}.${REPO_SUFFIX}/$IMAGE:$VERSION
    done
fi

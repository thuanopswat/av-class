import os
import sys
path = os.path.dirname(os.path.abspath(__file__))
libpath = os.path.join(path, 'lib/')
sys.path.insert(0, libpath)
from avclass_common import AvLabels
from operator import itemgetter
class ThreatNameGenerator:
    # Default alias file
    default_alias_file = os.path.join(path, "data/default.aliases")
    # Default generic tokens file
    default_gen_file = os.path.join(path, "data/default.generics")
    # Default generic tokens files
    default_gen_files = []
    default_gen_files.append(os.path.join(path, "data/default.generics"))
    default_gen_files.append(os.path.join(path, "data/default.banned"))
    # Default families file
    default_type_file = os.path.join(path, "data/default.types")
    default_malware_types = os.path.join(path, "data/default.malware_types")
    UNKNOWN = 'Unknown'

    # Generate families dictionary
    malware_types = AvLabels.read_families(default_type_file)
    av_labels = AvLabels(default_gen_files, default_alias_file)

    malware_t = AvLabels.get_malware_types()

    @staticmethod
    def guess_hash(h):
        '''Given a hash string, guess the hash type based on the string length'''
        hlen = len(h)
        if hlen == 32:
            return 'md5'
        elif hlen == 40:
            return 'sha1'
        elif hlen == 64:
            return 'sha256'
        else:
            return None

    @staticmethod
    def get_hash_type(data):
        for hash_type in ['sha1','md5','sha256']:
            if hash_type in data['file_info']:
                return hash_type

        return None

    @staticmethod
    def generate(data, pup = False):
        hash_type = ThreatNameGenerator.get_hash_type(data)
        first_token_dict = {}

        from_vt = False
        ifile_are = True
        sample_info = ThreatNameGenerator.av_labels.get_sample_info(data, ifile_are, from_vt)
        if sample_info is None:
            try:
                name = data['file_info'][hash_type]
            except KeyError:
                print("Treat this error")

        name = getattr(sample_info, hash_type)
        # unique_id = getattr(sample_info, 'id')[-6:].lower()

        # If the report has no AV labels, continue
        if not sample_info[3]:
            family = ''

        # Get the distinct tokens from all the av labels in the report
        # And print them. If not verbose, print the first token.
        # If verbose, print the whole list
        try:
            # Get distinct tokens from AV labels
            labels = []

            tokens = ThreatNameGenerator.av_labels.get_family_ranking(sample_info)
            # Top candidate is most likely family name
            if tokens:
                family = tokens[0][0]
                is_singleton = False
            else:
                family = "SINGLETON:" + name
                is_singleton = True

            # Build family map for precision, recall, computation
            first_token_dict[name] = family
            if family.lower() in ThreatNameGenerator.malware_types:
                malware_type = ThreatNameGenerator.malware_types[family]
                for token in tokens:
                    if len(labels) > 2:
                        break
                    else:
                        if token[0] in ThreatNameGenerator.malware_t:
                            labels.append(token[0])
                if len(labels) == 0:
                    labels = [ThreatNameGenerator.UNKNOWN.lower()]
            else:
                malware_type = ThreatNameGenerator.UNKNOWN
                labels = [malware_type.lower()]



            if malware_type.lower() == family:
                if tokens and len(tokens) > 1:
                    for token in sorted(tokens, key=itemgetter(1, 0), reverse=False):
                        if token[0] not in ThreatNameGenerator.malware_t:
                            family = token[0]
                            continue
                else:
                    family = ThreatNameGenerator.UNKNOWN



            if is_singleton or family == 'suspicious':
                family = ThreatNameGenerator.UNKNOWN
                malware_type = 'PUA'
                labels = [malware_type.lower()]
            threat_name = '%s/%s' % (malware_type, family.capitalize())

            response = {
                "malware_type": labels,
                "malware_family": family,
                "threat_name": threat_name
            }

            return response
        except:
            print("Treat another exception")
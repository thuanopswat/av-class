import json
import zerorpc
from threat_name_generator import ThreatNameGenerator
from datetime import datetime

print('SERVICE AVCLASS STARTED!',datetime.now())

class AVClass(object):
    def av_class(self, file_content):
        try:
            file_content = json.loads(file_content)
        except:
            pass

        return ThreatNameGenerator.generate(file_content)

    def healthcheck(self):
        return {
            "success": True
        }

s = zerorpc.Server(AVClass())
s.bind("tcp://0.0.0.0:7001")
s.run()
